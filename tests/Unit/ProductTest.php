<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\model\Product;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testHasItemInProduct()
    {
        $product = new Product(['cat', 'toy', 'torch']);

        $this->assertTrue($product->has('toy'));
        $this->assertFalse($product->has('ball'));
    }
}

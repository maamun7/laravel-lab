<?php

use Illuminate\Database\Seeder;
use App\model\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\model\Product::class, 100)->create();
    }
}
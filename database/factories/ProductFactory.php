<?php

use Faker\Generator as Faker;

$factory->define(App\model\Product::class, function (Faker $faker) {
    $name = $faker->name;
    $sku = rand(1000,9999);
    return [
        'name'          => $name,
        'model'         => $faker->word,
        'sku'           => $faker->unique->word,
        'slung'         => strtolower(str_replace(" ","-",$name)),
        'image'         => $faker->imageUrl($width = 200, $height = 200),
        'description'   => $faker->sentence(5),
        'price'         => rand(5000,10000)
    ];

});
